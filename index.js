const topics = [...new Set(["Water", "Stars", "DNA", "Molecules", "Atoms", "Protons", "Electrons", "Nucleus", "Hydrogen", "Oxygen", "Carbon", "Nitrogen", "clouds", "rain", "snow", "ice", "steam", "cats", "dogs", "birds", "fish", "snakes", "cells", "plants", "trees", "flowers", "bacteria", "viruses", "fungi", "mushrooms", "algae", "insects", "spiders", "worms", "reptiles", "amphibians", "crustaceans", "mollusks", "Neutrons", "Gravity", "Magnetism", "Electricity", "Light", "Sound", "Heat", "Time", "Space", "Universe", "Galaxy", "rocks"])];

console.log(JSON.stringify(topics.sort()));
console.log(topics.length);
